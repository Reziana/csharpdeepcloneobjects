﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using testDeepCopy.Model;

namespace testDeepCopy
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private HeaderObject HeaderObject = new HeaderObject() { HeaderID=1,HeaderName="nameH",HeaderNote="im note" };
        public MainWindow()
        {
            InitializeComponent();

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            HeaderObject.Details = new List<details>();
            HeaderObject.Details.Add(new details() {dtlID=1,dtlName="dtl1",dtlNote="ntt",nrTest=1 });
            HeaderObject.Details.Add(new details() {dtlID=2,dtlName="dtl2",dtlNote="2n",nrTest=2 });
            HeaderObject.Details.Add(new details() {dtlID=3,dtlName="dtl3",dtlNote="3n",nrTest=3 });
            HeaderObject.Details.Add(new details() {dtlID=4,dtlName="dtl4",dtlNote="4n",nrTest=4 });
            var cloneobj = HeaderObject.Clone;
            cloneobj.HeaderName = "cloneName";
            cloneobj.Details[0].nrTest = 55555;
           _labIsTrue.Content = cloneobj.Details[0].nrTest.Equals(HeaderObject.Details[0].nrTest) ? "the clone is not createt" : "Clone is created";
         
             


        }
    }
}
