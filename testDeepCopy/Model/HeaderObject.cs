﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testDeepCopy.Helpers;

namespace testDeepCopy.Model
{
  //  [Serializable]
    public class HeaderObject
    {
       
        public HeaderObject()
        {
            Details = new List<details>();
        }

        public int HeaderID { get; set; }
        public string HeaderName { get; set; }
        [JsonIgnore]
        public string HeaderNote { get; set; }
        public List<details> Details { get; set; }

     [JsonIgnore]
        public HeaderObject Clone { get {
               HeaderObject clone = this.Clone();
               clone.HeaderNote = this.HeaderNote;
                 return clone;
        } }

    }
}
